import json
import iso8601
import pytz
from datetime import datetime, timedelta

# user configuration -> change this properties to install
# -------------------------------------------------------

# Defines if local files will be taken (developer mode)

DATA_IMPORT_DEV_MODE = False

# folder where the .txt files are stored
SRS_DATA_FOLDER = 'data/srs'
EVENTS_DATA_FOLDER = 'data/events'

# ftp server informations
SWPC_WAREHOUSE_FTP = 'ftp.swpc.noaa.gov'
SWPC_WAREHOUSE_ROOT = '/pub/warehouse/'

SWPC_SRS_REPORT_DIR = 'pub/warehouse/%s/SRS/' % datetime.now().strftime('%Y')
SWPC_EVENTS_REPORT_DIR = 'pub/warehouse/%s/%s_events/' % (
    datetime.now().strftime('%Y'), datetime.now().strftime('%Y'))

params = None
with open("params.json") as f:
    params = json.loads(f.read())

DB_HOST = params["services"]["db"]["read"]["host"]
DB_PORT = params["services"]["db"]["read"]["port"]
DB_USER = params["services"]["db"]["read"]["user"]
DB_PASSWORD = params["services"]["db"]["read"]["password"]
DB_NAME = params["algorithm"]["db"]["name"]
SWPC_SRS_TABLE = params["algorithm"]["db"]["swpc_srs_table"]
SWPC_EVENT_TABLE = params["algorithm"]["db"]["swpc_event_table"]

#START = iso8601.parse_date(params["algorithm"]["start_time"])
#END = iso8601.parse_date(params["algorithm"]["end_time"])
START = datetime.now(pytz.utc) - timedelta(days=5)
END = datetime.now(pytz.utc) + timedelta(days=1)
