# combines two lists together to a dictionary
import fnmatch
import os

__META_DATA_FIELDS = {'nar': 'int',
                      'npr': 'int',
                      'lat_hg': 'int',
                      'npr_lat_hg': 'int',
                      'long_hg': 'int',
                      'npr_long_hg': 'int',
                      'long_carr': 'float',
                      'npr_long_carr': 'float',
                      'dlong_hg': 'int'}


def combine_lists(list1, list2):
    return dict(zip(list1, list2))


def merge_two_dicts(x, y):
    """Given two dicts, merge them into a new dict as a shallow copy."""
    z = x.copy()
    z.update(y)
    return z


# returns text of a file
def read_all_text(filename):
    data_file = open(filename, 'r')
    text = data_file.read()
    data_file.close()
    return text


def recursive_glob(treeroot, pattern):
    results = []
    for base, dirs, files in os.walk(treeroot):
        goodfiles = fnmatch.filter(files, pattern)
        results.extend(os.path.join(base, f) for f in goodfiles)
    return results


def convert_coordinate_axis(axis_value):
    if axis_value is None:
        return None

    if axis_value[0] == 'S' or axis_value[0] == 'E':
        return '-' + axis_value[1:3]
    else:
        return axis_value[1:3]


def group_by(key, values):
    types = set(map(lambda e: e[key], values))
    return {k: filter(lambda e: e[key] == k, values) for k in types}


def convert_data_types(key, value):
    data_type = __META_DATA_FIELDS.get(key, None)

    if data_type is None or value is None:
        return value

    if data_type == 'int':
        return int(value)

    if data_type == 'float':
        return float(value)
