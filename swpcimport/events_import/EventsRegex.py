import re

# https://regex101.com/r/rW8eL8/2
EVENT_DATE_PATTERN = re.compile(ur'''
for\s
(?P<date>
  (?P<year>\d{4})
  \s
  (?P<month>(?:\w{3}))
  \s
  (?P<day>\d{2})
)
''', re.VERBOSE)

# https://regex101.com/r/aC8hH1/13
EVENT_TABLE_PATTERN = re.compile(ur'''
# Parse table of SWPC event data
(?P<event>\d{2,4})
\s+
(?P<multi_event_flag>\+)?
\s+
(?P<time_start_flag>\w)?
(?P<time_start>\d{4})
\s+
(?P<time_peak_flag>\w)?
(?:
  (?P<time_peak>\d{4})|
  /{4}
)
\s+
(?P<time_end_flag>\w)?
(?P<time_end>\d{4})
\s+
(?P<observatory>\w{3})
\s+
(?P<quality>\w)
\s+
(?P<type>\w{3})
\s+
(?:
  (?P<location>
    (?P<lat_hg>\w\d{2})
    (?P<long_hg>\w\d{2})
  )|
  (?P<wavelength>
    (?P<wl_from>\d+)
    \-
    (?P<wl_to>\d+[A])
  )|
  (?P<frequency_range>
    (?P<freq_from>\d+)
    \-
    (?P<freq_to>\d+)
  )|
  (?P<frequency>
    (\d{3,5})
  )|
  (?P<no_freq>
    (/{4})
  )
)
\s+
(?P<particulars_1>[\w/\.\+]+)
\s+
(?:
  (?P<particulars_2>
    (?:\d\.\d\w[\-\+]\d{2})?
  )|
  (?P<particulars_3>\w{3}?)|
  ()
)
\s+
(?:
  (?P<regnum>\d{4})|
  ()
)
\n
''', re.VERBOSE)
