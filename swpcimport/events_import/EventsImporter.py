from datetime import datetime, timedelta
import iso8601
import json
import re
from sirql.SiRQLParser import SiRQLParser

from swpcimport.config import START, END
from swpcimport.SWPCDataDownloader import download_data_from_ftp
from swpcimport.SWPCUtil import recursive_glob, read_all_text, group_by, \
    merge_two_dicts, convert_coordinate_axis
from swpcimport.config import EVENTS_DATA_FOLDER
from swpcimport.events_import.EventTransformer import transform_event
from swpcimport.events_import.EventsRegex import EVENT_DATE_PATTERN, \
    EVENT_TABLE_PATTERN
from swpcimport.SWPCDataAccessObject import SWPCDataAccessObject

# Define global variables
SWPC_EVENT_FIELDS = [
    'file_creation', 'event', 'multi_event_flag', 'time_start', 'time_peak',
    'time_end', 'observatory', 'quality', 'type', 'lat_hg', 'long_hg',
    'frequency', 'wavelength', 'particulars_1', 'particulars_2',
    'particulars_3', 'regnum'
]


def import_events(dev_mode=False):
    if dev_mode:
        new_files = recursive_glob(EVENTS_DATA_FOLDER, '*.txt')
    else:
        # start downloading the new EVENT files.
        print('downloading new event files...')
        if START <= END:
            new_files = download_data_from_ftp(
                EVENTS_DATA_FOLDER, 'events', [START, END]
            )

    events_per_day = map(parse_data, map(read_all_text, new_files))

    # delete not parsed events
    fixed_events_per_day = filter(lambda e: e, events_per_day)

    flat_events = [item for sublist in fixed_events_per_day for item in
                   sublist]

    fixed_flat_events = map(fix_coordinates, flat_events)
    real_events = map(transform_event, fixed_flat_events)
    grouped_events = group_by('type', real_events)

    deleted_entries = 0
    imported_entries = len(real_events)
    with SWPCDataAccessObject(auto_commit=False) as ctx:
        # the last three days of event data are not stable and may
        # have changed!
        # current strategy:
        # - remove all previous data entries
        # - re-import new data entries
        sirql_conditions = SiRQLParser.parse({
            "file_creation":
            "between('%s', '%s')" % (
                START - timedelta(seconds=1),
                END + timedelta(minutes=31)
            )
        })
        deleted_entries = ctx.delete_swpc_event_entries_by_sirql(
            sirql_conditions
        )

        for swpc_event_entries in grouped_events.values():
            for swpc_event_entry in swpc_event_entries:
                # correct SWPC EVENT fields
                swpc_event_entry.update({
                    'multi_event_flag':
                        True if (
                            swpc_event_entry.pop('multi_event_flag') == '+'
                        ) else False
                })
                if swpc_event_entry['frequency_range'] is None:
                    swpc_event_entry.update(
                        {"frequency": swpc_event_entry.pop("frequency_range")}
                    )
                if swpc_event_entry['particulars_2'] == '':
                    swpc_event_entry['particulars_2'] = None

                # store the SWPC EVENT data
                ctx.add_swpc_event_entry(**{
                    k: v for k, v in swpc_event_entry.items()
                    if k in SWPC_EVENT_FIELDS
                })

        ctx.commit()

    print(
        (
            'import completed\r\n' +
            '    time interval: %s - %s\r\n' +
            '    old entries removed: %s\r\n' +
            '    new entries imported: %s'
        ) % (
            START, END,
            deleted_entries, imported_entries
        )
    )

    # the following condition is valid!
    # given, an entry was removed retrospectively
    if imported_entries < deleted_entries:
        print('WARNING: deleted more srs data than imported!')


# parses text with regex
def parse_data(text):
    date_result = re.finditer(EVENT_DATE_PATTERN, text)
    rows_result = re.finditer(EVENT_TABLE_PATTERN, text)

    date_match = next((x for x in date_result), None)

    if date_match is None:
        return None

    date = datetime.strptime(date_match.group('date'), '%Y %b %d')
    events = map(lambda m: m.groupdict(), rows_result)

    # append dates to event
    events_with_date = map(
        lambda e: merge_two_dicts(e, {
            'file_creation': date,
        }),
        events
    )

    # add date to timestamps
    events_with_timestamps = map(parse_event_timestamps, events_with_date)
    events_with_timestamps = filter(
        lambda a: a is not None, events_with_timestamps
    )

    return events_with_timestamps


def parse_event_timestamps(event):
    date = event["file_creation"].date()

    keys = ['time_start', 'time_peak', 'time_end']
    keys = list(filter(lambda k: event[k] is not None, keys))
    try:
        timestamps = map(lambda k: datetime.strptime(event[k], '%H%M'), keys)
    except ValueError:
        print("WARNING: Invalid timestamps: %s %s " % (date, ", ".join(map(lambda k: event[k], keys))))
        return None

    timestamps = map(lambda t: datetime.combine(date, t.time()), timestamps)
    timestamps = list(timestamps)

    if timestamps[-1] < timestamps[0]:
        timestamps[-1] += timedelta(days=1)
    if timestamps[-2] < timestamps[0]:
        timestamps[-2] += timedelta(days=1)

    if not timestamps == list(sorted(timestamps)):
        return None

    for k, t in zip(keys, timestamps):
        event[k] = t.isoformat() + "Z"

    return event


def fix_coordinates(data):
    data['long_hg'] = convert_coordinate_axis(data['long_hg'])
    data['lat_hg'] = convert_coordinate_axis(data['lat_hg'])
    return data
