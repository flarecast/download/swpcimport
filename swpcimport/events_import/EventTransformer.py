EVENT_TYPES = 'BSL', 'DSF', 'EPL', 'FIL', 'FLA', 'FOR', 'GLE', 'LPS', 'PCA', \
              'RBR', 'RNS', 'RSP', 'SPY', 'XFL', 'XRA'


def transform_event(event):
    switch = {'XRA': to_xra, 'FLA': to_fla, 'XFL': to_xfl, 'RBR': to_rbr,
              'RSP': to_rsp}
    event_type = event['type']

    if event_type in switch.keys():
        # if transformation fails just skip it
        try:
            event = switch[event_type](event)
        except Exception:
            return event

    return event


def to_xra(event):
    specific = {'xray_class': event['particulars_1'],
                'optical_class': event['particulars_2']}

    event['specific'] = specific
    return event


def to_fla(event):
    specific = {'importance': event['particulars_1'][0],
                'brightness': event['particulars_1'][1],
                'characteristics': event['particulars_3']}
    event['specific'] = specific
    return event


def to_xfl(event):
    # there are no events yet

    specific = {'importance': event['particulars_1'][0]}
    event['specific'] = specific
    return event


def to_rbr(event):
    specific = {'peak': event['particulars_1']}
    event['specific'] = specific
    return event


def to_rsp(event):
    specific = {}
    data = event['particulars_1']
    specific['type'] = data[0:data.index('/')]
    specific['intensity'] = data[data.index('/') + 1:]
    specific['shock_speed'] = event['particulars_3']
    event['specific'] = specific
    return event
