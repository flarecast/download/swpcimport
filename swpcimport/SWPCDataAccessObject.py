import psycopg2
from swpcimport import config
from psycopg2.extras import RealDictCursor
from psycopg2.extensions import AsIs

from sirql import SiRQLToSql


class SWPCDataAccessObject(object):
    """
    Data access object for connecting to the 'staging' schema of a postgres
    database. SWPCDataAccessObject is a wrapper class which uses psycopg2 as
    database adapter. Example using the "which" statement (recommended):

        with SWPCDataAccessObject(auto_commit=False) as ctx:
            swpc_srs_entries = ctx.get_swpc_srs_entries(
                ['*'], {'nar': '2472'}
            )
    """
    def __init__(self, auto_commit=True):
        """
        Creates a new SWPCDataAccessObject.
        The parameter auto_commit defines if a commit is send to the database
        whenever an update or insert method is called. If auto_commit is set
        to 'False' the SWPCDataAccessObject.commit() method has to be called
        manually at the end of a transaction.
        More parameters are obtained by the swpcimport/config.py file:
        - database name
        - user name (login)
        - password (login)

        :param auto_commit: enables/disables auto commits (default=True)
        """
        self.__con = psycopg2.connect(
            host=config.DB_HOST,
            database=config.DB_NAME,
            port=config.DB_PORT,
            user=config.DB_USER,
            password=config.DB_PASSWORD
        )

        self.__con.cursor_factory = RealDictCursor
        self.__con.autocommit = auto_commit
        self.__cur = self.__con.cursor()

    def __enter__(self):
        """
        Method which is called when entering a "which" statement.

        :return: self
        """
        return self

    def __exit__(self, exception_type, exception_value, traceback):
        """
        Method which is called when leaving a "which" statement.
        Hereby, the cursor as well as the database connection
        will be closed.
        """
        self.__con.rollback()
        self.__cur.close()
        self.__con.close()

    @property
    def connection(self):
        """
        Method to obtain the (raw) database connection.

        :return: database connection
        """
        return self.__con

    @property
    def cursor(self):
        """
        Method to obtain the (raw) database cursor.

        :return: database cursor
        """
        return self.__cur

    # --- public ---

    # workflow
    def commit(self):
        """
        Method which commits a pending database transaction.
        """
        self.__con.commit()

    # add
    def add_swpc_srs_entry(
        self,
        file_creation, data_received,
        nar=None, npr=None,
        lat_hg=None, npr_lat_hg=None,
        long_hg=None, npr_long_hg=None,
        long_carr=None, npr_long_carr=None,
        area=None, mcint_zur=None, mcint_pen=None, mcint_com=None,
        dlong_hg=None, n_spots=None, mtwil_class=None
    ):
        """
        Method which adds a SWPC SRS entry to the database.

        :param file_creation: creation date of the SRS file
        :param data_received: reception date of the data at
            Space Weather Operations (SWO)
        :param nar: NOAA active region number of the entry (optional)
        :param npr: NOAA plage region number of the entry (optional)
        :param lat_hg:
            latitude (in heliographic degrees) of the entry (optional)
        :param npr_lat_hg:
            latitude (in heliographic degrees) of the plage entry (optional)
        :param long_hg:
            longitude (in heliographic degrees) of the entry (optional)
        :param npr_long_hg:
            longitude (in heliographic degrees) of the plage entry (optional)
        :param long_carr:
            longitude (in carrington degrees) of the entry (optional)
        :param npr_long_carr:
            longitude (in carrington degrees) of the plage entry (optional)
        :param area: area of the entry (optional)
        :param mcint_zur:
            modified Zurich class (McIntosh schema) of the entry (optional)
        :param mcint_pen:
            penumbral class (McIntosh schema) of the entry (optional)
        :param mcint_com:
            compactness class (McIntosh schema) of the entry (optional)
        :param dlong_hg:
            longitudinal extent of the group (in heliographic degrees)
            of the entry (optional)
        :param n_spots:
            number of visible sunspots in the group of the entry (optional)
        :param mtwil_class:
            magnetic class (Mount Wilson Observatory) of the entry (optional)
        :return: ID of the new SWPC SRS entry
        """
        self.__cur.execute(
            ("INSERT INTO " + config.SWPC_SRS_TABLE + " (" +
             "    id, timestamp, file_creation, data_received, nar, npr," +
             "    lat_hg, npr_lat_hg, long_hg, npr_long_hg, " +
             "    long_carr, npr_long_carr, area, mcint_zur, mcint_pen," +
             "    mcint_com, dlong_hg, n_spots, mtwil_class" +
             ") VALUES (" +
             "    %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s,"
             "    %s, %s, %s, %s, %s" +
             ") RETURNING id"),
            (AsIs('DEFAULT'), AsIs('DEFAULT'),
             file_creation, data_received, nar, npr, lat_hg, npr_lat_hg,
             long_hg, npr_long_hg, long_carr, npr_long_carr,
             area, mcint_zur, mcint_pen, mcint_com, dlong_hg, n_spots,
             mtwil_class)
        )
        ret = self.__cur.fetchone()['id']
        if self.__con.autocommit:
            self.__con.commit()
        return ret

    def add_swpc_event_entry(
        self,
        file_creation,
        event=None, multi_event_flag=None, time_start=None, time_peak=None,
        time_end=None, observatory=None, quality=None, type=None, lat_hg=None,
        long_hg=None, frequency=None, wavelength=None, particulars_1=None,
        particulars_2=None, particulars_3=None, regnum=None,
    ):
        """
        Method which adds a SWPC EVENT entry to the database.

        :param file_creation: creation date of the EVENT file
        :param event: event number (optional)
        :param multi_event_flag: flag for multiple events (optional)
        :param time_start: start of the event (optional)
        :param time_peak: maximum of the event (optional)
        :param time_end: end of the event (optional)
        :param observatory: reporting observatory for the entry (optional)
        :param quality: quality of the entry data (optional)
        :param type: report type of the entry (optional)
        :param lat_hg:
            latitude (in heliographic degrees) of the entry (optional)
        :param long_hg:
            longitude (in heliographic degrees) of the entry (optional)
        :param frequency: frequency of the entry (optional)
        :param wavelength: wavelength of the entry (optional)
        :param particulars_1:
            Additional report information (1) for the entry (optional)
        :param particulars_2:
            Additional report information (2) for the entry (optional)
        :param particulars_3:
            Additional report information (3) for the entry (optional)
        :param regnum:
            SWPC-assigned solar region number for the entry (optional)
        :return: ID of the new SWPC EVENT entry
        """
        self.__cur.execute(
            ("INSERT INTO " + config.SWPC_EVENT_TABLE + " (" +
             "    id, timestamp, file_creation, event, multi_event_flag," +
             "    time_start, time_peak, time_end, observatory, quality," +
             "    type, lat_hg, long_hg, frequency, wavelength," +
             "    particulars_1, particulars_2, particulars_3, regnum" +
             ") VALUES (" +
             "    %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s," +
             "    %s, %s, %s, %s, %s" +
             ") RETURNING id"),
            (AsIs('DEFAULT'), AsIs('DEFAULT'),
             file_creation, event, multi_event_flag, time_start, time_peak,
             time_end, observatory, quality, type, lat_hg, long_hg,
             frequency, wavelength, particulars_1, particulars_2,
             particulars_3, regnum)
        )
        ret = self.__cur.fetchone()['id']
        if self.__con.autocommit:
            self.__con.commit()
        return ret

    # get
    def get_swpc_srs_entries(
        self, fields=["*"], where={}, order_by={}, limit=None
    ):
        """
        Method which gets a list of SWPC SRS entries.

        :param fields: list of field names
        :param where: dictionary of conditions
        :param order_by: sort order
        :param limit: maximum number of entries to obtain
        :return: RealDictCursor to the entities
        """
        return self._get(config.SWPC_SRS_TABLE, fields, where, order_by, limit)

    def get_swpc_event_entries(
        self, fields=["*"], where={}, order_by={}, limit=None
    ):
        """
        Method which gets a list of SWPC EVENT entries.

        :param fields: list of field names
        :param where: dictionary of conditions
        :param order_by: sort order
        :param limit: maximum number of entries to obtain
        :return: RealDictCursor to the entities
        """
        return self._get(
            config.SWPC_EVENT_TABLE, fields, where, order_by, limit
        )

    def _get(self, table, fields=["*"], where={}, order_by={}, limit=None):
        """
        Method which gets a list of entities.

        :param table: the table to get entities from
        :param fields: list of field names
        :param where: dictionary of conditions
        :param order_by: sort order
        :param limit: maximum number of entries to obtain
        :return: RealDictCursor to the entities
        """
        if limit is None:
            limit = AsIs('ALL')

        self.__cur.execute(
            "SELECT " + ",".join("%s" for f in fields) + " " +
            "FROM " + table + " " +
            "WHERE True" + "".join(" AND %s=%s" for k in where.keys()) + " " +
            "ORDER BY " + "".join("%s %s," for k in order_by.keys()) + "id " +
            "LIMIT %s",
            (
                tuple([AsIs(f) for f in fields]) +
                sum([(AsIs(key), where[key]) for key in where.keys()], ()) +
                sum([(
                        AsIs(key), AsIs(order_by[key])
                    ) for key in order_by.keys()], ()) +
                (limit,)
            )
        )
        return self.__cur.fetchall()

    # get by sirql
    def get_swpc_srs_entries_by_sirql(
        self, fields=["*"], sirql_conditions=[], order_by={}, limit=None
    ):
        """
        Method which gets a list of SWPC SRS entries.

        :param fields: list of field names
        :param sirql_conditions: list of sirql conditions
        :param order_by: sort order
        :param limit: maximum number of entries to obtain
        :return: RealDictCursor to the entities
        """
        return self._get_by_sirql(
            config.SWPC_SRS_TABLE, fields, sirql_conditions, order_by, limit
        )

    def get_swpc_event_entries_by_sirql(
        self, fields=["*"], sirql_conditions=[], order_by={}, limit=None
    ):
        """
        Method which gets a list of SWPC EVENT entries.

        :param fields: list of field names
        :param sirql_conditions: list of sirql conditions
        :param order_by: sort order
        :param limit: maximum number of entries to obtain
        :return: RealDictCursor to the entities
        """
        return self._get_by_sirql(
            config.SWPC_EVENT_TABLE, fields, sirql_conditions, order_by, limit
        )

    def _get_by_sirql(
        self,
        table, fields=["*"], sirql_conditions=[], order_by={}, limit=None
    ):
        """
        Method which gets a list of entities.

        :param table: the table to get entities from
        :param fields: list of field names
        :param sirql_conditions: list of sirql conditions
        :param order_by: sort order
        :param limit: maximum number of entries to obtain
        :return: RealDictCursor to the entities
        """
        if limit is None:
            limit = AsIs('ALL')

        where = "True"
        if len(sirql_conditions) > 0:
            where = SiRQLToSql.convert(sirql_conditions)

        self.__cur.execute(
            "SELECT " + ",".join("%s" for f in fields) + " " +
            "FROM " + table + " " +
            "WHERE %s " +
            "ORDER BY " + "".join("%s %s," for k in order_by.keys()) + "id " +
            "LIMIT %s",
            (
                tuple([AsIs(f) for f in fields]) +
                (AsIs(where),) +
                sum([(
                        AsIs(key), AsIs(order_by[key])
                    ) for key in order_by.keys()], ()) +
                (limit,)
            )
        )
        return self.__cur.fetchall()

    # get (user defined)
    pass

    # delete
    def delete_swpc_srs_entries(self, where={}):
        """
        Method which deletes SWPC SRS entries.

        :param where: dictionary of conditions
        :return: Number of deleted rows
        """
        return self._delete(config.SWPC_SRS_TABLE, where)

    def delete_swpc_event_entries(self, where={}):
        """
        Method which deletes SWPC EVENT entries.

        :param where: dictionary of conditions
        :return: Number of deleted rows
        """
        return self._delete(config.SWPC_EVENT_TABLE, where)

    def _delete(self, table, where={}):
        """
        Method which deletes entities.

        :param table: the table to delete entities from
        :param where: dictionary of conditions
        :return: Number of deleted rows
        """
        self.__cur.execute(
            "DELETE FROM " + table + " " +
            "WHERE True " + "".join("AND %s=%s " for k in where.keys()),
            (
                sum([(AsIs(key), where[key]) for key in where.keys()], ())
            )
        )

        ret = self.__cur.rowcount
        if self.__con.autocommit:
            self.__con.commit()
        return ret

    # delete by sirql
    def delete_swpc_srs_entries_by_sirql(self, sirql_conditions=[]):
        """
        Method which deletes SWPC SRS entries.

        :param sirql_conditions: list of sirql conditions
        :return: Number of deleted rows
        """
        return self._delete_by_sirql(
            config.SWPC_SRS_TABLE, sirql_conditions
        )

    def delete_swpc_event_entries_by_sirql(self, sirql_conditions=[]):
        """
        Method which deletes SWPC EVENT entries.

        :param sirql_conditions: list of sirql conditions
        :return: Number of deleted rows
        """
        return self._delete_by_sirql(
            config.SWPC_EVENT_TABLE, sirql_conditions
        )

    def _delete_by_sirql(self, table, sirql_conditions=[]):
        """
        Method which deletes entities.

        :param table: the table to delete entities from
        :param sirql_conditions: list of sirql conditions
        :return: Number of deleted rows
        """
        where = "True"
        if len(sirql_conditions) > 0:
            where = SiRQLToSql.convert(sirql_conditions)

        self.__cur.execute(
            "DELETE FROM " + table + " " +
            "WHERE %s",
            (
                AsIs(where),
            )
        )

        ret = self.__cur.rowcount
        if self.__con.autocommit:
            self.__con.commit()
        return ret

    # update
    pass

    # --- private ---

    pass
