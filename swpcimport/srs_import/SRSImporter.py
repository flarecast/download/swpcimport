from datetime import datetime, timedelta
import iso8601
import json
import re
from sirql.SiRQLParser import SiRQLParser

from swpcimport.config import START, END
from swpcimport.config import SRS_DATA_FOLDER
from swpcimport.SWPCDataDownloader import download_data_from_ftp
from swpcimport.SWPCUtil import recursive_glob, read_all_text, \
    merge_two_dicts, convert_coordinate_axis, convert_data_types
from swpcimport.srs_import.SRSRegex import SRS_ISSUE_PATTERN, \
    SRS_DATE_PATTERN, SRS_TABLE_PATTERN
from swpcimport.SWPCDataAccessObject import SWPCDataAccessObject

# Define global variables
MOUNT_WILSON_SCALE = ['None', 'Alpha', 'Beta', 'Gamma', 'Beta-Gamma',
                      'Beta-Delta', 'Gamma-Delta', 'Beta-Gamma-Delta']


def import_srs(dev_mode=False):
    if dev_mode:
        print('reading text files...')
        raw_files = recursive_glob(SRS_DATA_FOLDER, '*.txt')
    else:
        # Start downloading the new SRS files.
        print('downloading new srs files...')
        if START <= END:
            raw_files = download_data_from_ftp(
                SRS_DATA_FOLDER, 'SRS', [START, END]
            )

    # process data (filter empty text)
    parsed_data = map(parse_data, filter(lambda t: len(t) > 0,
                                         map(read_all_text, raw_files)))

    # create flat list
    flat_data = [item for sublist in parsed_data for item in sublist]

    # fix data
    fixed_flat_data = map(fix_mount_wilson_scale,
                          map(fix_coordinates, flat_data))

    # convert data types
    converted_data = map(lambda x: {k: convert_data_types(
        k, v) for k, v in x.items()}, fixed_flat_data)

    deleted_entries = 0
    imported_entries = len(converted_data)
    with SWPCDataAccessObject(auto_commit=False) as ctx:
        # the last three days of event data are not stable and may
        # have changed!
        # current strategy:
        # - remove all previous data entries
        # - re-import new data entries
        sirql_conditions = SiRQLParser.parse({
            "file_creation":
            "between('%s', '%s')" % (
                START - timedelta(seconds=1),
                END + timedelta(minutes=31)
            )
        })
        deleted_entries = ctx.delete_swpc_srs_entries_by_sirql(
            sirql_conditions
        )

        # store the SWPC SRS data
        for swpc_srs_entry in converted_data:
            ctx.add_swpc_srs_entry(**swpc_srs_entry)

        ctx.commit()

    print(
        (
            'import completed\r\n' +
            '    time interval: %s - %s\r\n' +
            '    old entries removed: %s\r\n' +
            '    new entries imported: %s'
        ) % (
            START, END,
            deleted_entries, imported_entries
        )
    )

    # the following condition is valid!
    # given, an entry was removed retrospectively
    if imported_entries < deleted_entries:
        print('WARNING: deleted more srs data than imported!')


# parses text with regex
def parse_data(text):
    issue = re.search(SRS_ISSUE_PATTERN, text)
    issue_date = datetime.strptime(issue.group(1), '%Y %b %d %H%M')

    received_dates = re.findall(SRS_DATE_PATTERN, text)
    rows_result = re.finditer(SRS_TABLE_PATTERN, text)

    received_date = try_find_correct_date(received_dates)
    srs_records = map(lambda m: m.groupdict(), rows_result)

    # append dates to record
    records_with_date = map(
        lambda e: merge_two_dicts(e, {
            'file_creation': issue_date.isoformat() + 'Z',
            'data_received': received_date.isoformat() + 'Z'
        }),
        srs_records
    )

    return records_with_date


# tries to find the correct date of the data
def try_find_correct_date(dates):
    # get right date (try to figure out receive year:
    # default is issue date - 1 day)
    tmp_issue_date_string = ' '.join(dates[0][1:len(dates[0])]).strip()
    tmp_computed_receive_date = ' '.join(dates[1][1:len(dates[1])]).strip()

    issue_date = datetime.strptime(tmp_issue_date_string, '%d %b %Y')
    appreciated_receive_date = issue_date - timedelta(days=1)

    computed_time_as_string = appreciated_receive_date.strftime('%d %b')

    # check if receive date could match
    if computed_time_as_string.lower() != tmp_computed_receive_date.lower():
        print('Wrong time appreciated: R: %s | A: %s' % (
            tmp_computed_receive_date, computed_time_as_string))

    # parse correct date into datetime
    received_date = datetime.strptime(tmp_computed_receive_date + ' ' + str(
        appreciated_receive_date.year), '%d %b %Y')
    return received_date


# recalculates the values of the longitude and latitude
# maps from N90/S90 to 90/-90 and E90/W90 to 90/-90
def fix_coordinates(data):
    data['long_hg'] = convert_coordinate_axis(data['long_hg'])
    data['npr_long_hg'] = convert_coordinate_axis(data['npr_long_hg'])
    data['lat_hg'] = convert_coordinate_axis(data['lat_hg'])
    data['npr_lat_hg'] = convert_coordinate_axis(data['npr_lat_hg'])
    return data


# adds 'None' to mt_wilson type if none was found
def fix_mount_wilson_scale(data):
    # check if mt_wilson is filled out
    if data['mtwil_class'] is None:
        data['mtwil_class'] = 'None'

    # convert string to upper case to match pre 2000 data
    data['mtwil_class'] = data['mtwil_class'].upper()
    return data
