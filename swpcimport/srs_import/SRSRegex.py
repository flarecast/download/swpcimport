import re

# https://regex101.com/r/i2X5fp/4
SRS_ISSUE_PATTERN = re.compile(
    ':Issued:\s((\d{4})\s(\w{3})\s(\d{2})\s(\d{4}))',
    re.IGNORECASE
)

# https://regex101.com/r/yB3qZ0/1
SRS_DATE_PATTERN = re.compile(
    'on\s((\d{2})\s(\w{3})(?:\w+)?(?:\s(\d{4}))?)',
    re.IGNORECASE
)

# https://regex101.com/r/rO8rD9/10
SRS_TABLE_PATTERN = re.compile(ur'''
(?P<nar>\d{4})
\s+
(?P<lat_hg>\w\d{2})
(?P<long_hg>\w\d{2})
\s+
(?P<long_carr>\d{3})
\s+
(?P<area>\d{4})
\s+
(?P<mcint_zur>\w)
(?P<mcint_pen>\w)
(?P<mcint_com>\w)
\s+
(?P<dlong_hg>\d{2})
\s+
(?P<n_spots>\d{2})
\ +
(?P<mtwil_class>[\w\-]*)
|
(?P<npr>\d{4})
\s+
(?P<npr_lat_hg>\w\d{2})
(?P<npr_long_hg>\w\d{2})
\s+
(?P<npr_long_carr>\d{3})
''', re.VERBOSE)
