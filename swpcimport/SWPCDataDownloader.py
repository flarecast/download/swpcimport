import ftplib
import json
import os
import sys
import iso8601
from tarfile import TarFile

from config import *


# downloads binary from ftp
def get_binary(ftp, filename, outfile=None):
    # fetch a binary file
    if outfile is None:
        outfile = sys.stdout
    ftp.retrbinary("RETR " + filename, outfile.write)
    outfile.close()


# main function for downloading new files from the ftp
def download_data_from_ftp(destination_folder, data_type, time_range):
    print('connecting to %s...' % SWPC_WAREHOUSE_FTP)

    start_date = time_range[0]
    end_date = time_range[1]

    start_year = int(start_date.strftime('%Y'))
    end_year = int(end_date.strftime('%Y'))

    # connect to ftp
    ftp = ftplib.FTP(SWPC_WAREHOUSE_FTP)
    ftp.login('anonymous', '')

    output_files = set()

    for year in range(start_year, end_year + 1):
        # root files
        ftp.cwd(SWPC_WAREHOUSE_ROOT)
        root_files = ftp.nlst()

        # check if year is available
        if str(year) not in root_files:
            print('Year %s not found on server!' % year)
            continue

        # change dir
        ftp.cwd(str(year))
        year_files = ftp.nlst()

        # check if zip
        data_zips = filter(lambda x: x.endswith('%s.tar.gz' % data_type),
                           year_files)
        if len(data_zips) > 0:
            # download and unzip
            zip_file = data_zips[0]
            zip_file_path = os.path.join(destination_folder, zip_file)

            print('downloading %s...' % zip_file),
            get_binary(ftp, zip_file, open(zip_file_path, 'wb'))

            print('extracting...'),
            with TarFile.open(zip_file_path) as tar:
                tar.extractall(path=destination_folder)

                # get root folder name
                tar_name = tar.getmembers()[0].name
            print('-> done')

            # find relevant files
            data_files_path = os.path.join(destination_folder, tar_name)
            files = os.listdir(data_files_path)
            relevant_files = filter(
                lambda file: is_file_relevant(file, start_date, end_date),
                files
            )
            [output_files.add(each) for each in
             map(lambda x: os.path.join(data_files_path, x),
                 relevant_files)]

        else:
            # download all files from folder
            data_folder = filter(lambda x: x.endswith(data_type), year_files)[
                0]
            ftp.cwd(data_folder)
            files = ftp.nlst()
            relevant_files = filter(
                lambda file: is_file_relevant(file, start_date, end_date),
                files
            )

            # download relevant files
            for rel_file in relevant_files:
                file_path = os.path.join(destination_folder, rel_file)
                print('downloading %s...' % rel_file),
                get_binary(ftp, rel_file, open(file_path, 'wb'))
                print('-> done')
                output_files.add(file_path)

    ftp.quit()
    print ('finished downloading')
    return output_files


def is_file_relevant(file_path, start_date, end_date):
    try:
        # get date from filename
        file_name = os.path.basename(file_path)
        date_string = str(file_name[:8])
        date = datetime.strptime(date_string, '%Y%m%d')
        date = date.replace(tzinfo=iso8601.UTC)

        # check if is between
        return start_date <= date <= end_date
    except Exception:
        return False
