FROM flarecast/python-base
MAINTAINER i4Ds

ADD . ./code

WORKDIR ./code

LABEL eu.flarecast.type="algorithm" \
      eu.flarecast.name="swpcimport" \
      eu.flarecast.algorithm.pipe="download"

CMD [ "python", "-B", "-u", "./main.py"]
