# Solar Weather Data Center (SWDC)

## SWPC Import
The SWPC data import is used to create a centralized data storage of relevant solar weather data.

##### Prerequisites
* Python version: 2.7.6
* psycopg2
* [pg4nosql](https://github.com/cansik/pg4nosql/)
* postgreSQL 9.4

### Configuration
The configuration of the script is stored in the **config.py** file. There you can set following settings:

* local file storage folder
* database connection information
* ftp server information

The parsing of the files is done with regex. The named capture group of the regex do also define the database column names!

```regex
...
(?P<num>\d{4})
\s+
(?P<long_hg>\w\d{2})
(?P<lat_hg>\w\d{2})
\s+
(?P<long_carr>\d{3})
\s+
(?P<area>\d{4})
\s+
...
```
*Regex example with capture group names.*

### Main Script
To start the SWPC Import run the script file **main.py**. It will run all subscripts.

The script automatically downloads data not yet available within the data storage.
Furthermore, data younger than 3 days are always re-imported as they are considered as instable.

### Solar Region Summary (SRS)
The subscript **SRSImporter.py** is used to import solar region data.

New files are downloaded from the SWPC Data Warehouse:
[ftp://ftp.swpc.noaa.gov/pub/warehouse/2015/SRS/](ftp://ftp.swpc.noaa.gov/pub/warehouse/2015/SRS/)

##### Readme
[ftp://ftp.swpc.noaa.gov/pub/forecasts/SRS/README](ftp://ftp.swpc.noaa.gov/pub/forecasts/SRS/README)

### Solar Events

New files are downloaded from the SWPC Data Warehouse:
[ftp://ftp.swpc.noaa.gov/pub/indices/events/](ftp://ftp.swpc.noaa.gov/pub/indices/events/)

##### Readme
[ftp://ftp.swpc.noaa.gov/pub/indices/events/README](ftp://ftp.swpc.noaa.gov/pub/indices/events/README)
