import os
import shutil

from swpcimport.config import DATA_IMPORT_DEV_MODE, \
    SRS_DATA_FOLDER, EVENTS_DATA_FOLDER
from swpcimport.events_import.EventsImporter import import_events
from swpcimport.srs_import.SRSImporter import import_srs


def main():
    print('loading global configuration...')

    print("creating dirs")
    if not DATA_IMPORT_DEV_MODE and os.path.exists(SRS_DATA_FOLDER):
        shutil.rmtree(SRS_DATA_FOLDER)
    if not DATA_IMPORT_DEV_MODE and os.path.exists(EVENTS_DATA_FOLDER):
        shutil.rmtree(EVENTS_DATA_FOLDER)

    create_directory(SRS_DATA_FOLDER)
    create_directory(EVENTS_DATA_FOLDER)

    print('importing srs (DEV=%s)...' % DATA_IMPORT_DEV_MODE)
    import_srs(dev_mode=DATA_IMPORT_DEV_MODE)

    print('importing events (DEV=%s)...' % DATA_IMPORT_DEV_MODE)
    import_events(dev_mode=DATA_IMPORT_DEV_MODE)


def create_directory(directory):
    if not os.path.exists(directory):
        os.makedirs(directory)


if __name__ == '__main__':
    main()
